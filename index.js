'use strict';

const esprima = require('esprima');


class JavaScriptCompiler {
  constructor(brunchCfg) {
    this.config = brunchCfg && brunchCfg.plugins && brunchCfg.plugins.javascript || {};
    this.validate = this.config.validate;
    if (this.validate == null) this.validate = true;
  }

  compile(params) {
    if (this.validate) {
      try {
        const errors = esprima.parse(params.data, {tolerant: true}).errors.map(error => error.message);
        if (errors.length) return Promise.reject(errors);
      } catch (error) {
        return Promise.reject(error);
      }
    }
    const path = params.path;
    //Добавляем регенератор только если не бовер, нпм или вендор
    var isBower = /bower_components/.test(path)
    var isNpm = /node_modules/.test(path)
    var isVendor = /vendor/.test(path) //TODO - надо проверять по конвенциям, иначе если кто-то допустит в пути файла этот
    //фрагмент, то регенератор не отработает
    if (!isBower && (!isNpm && !isVendor)) {
      params.data = require("regenerator").compile(params.data).code;
    }

    return Promise.resolve(params);
  }
}

JavaScriptCompiler.prototype.brunchPlugin = true;
JavaScriptCompiler.prototype.type = 'javascript';
JavaScriptCompiler.prototype.extension = 'js';

module.exports = JavaScriptCompiler;
